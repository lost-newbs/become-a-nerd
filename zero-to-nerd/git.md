# Git

We're learning git! Get ready.

Git is a must for any coding. It's a way to store code, see past revisions, and most importantly, collaborate with a team. 

Git provides ways to create branches of the code where edits can be made in their own little corner without affecting the main branch, which typically contains only tested code that is deployed into live applications.

Branches are accepted back into the main branch with what are called `merge requests` or `pull requests`, depending on where you're using git (we like GitLab, but GitHub is the most popular). Merge requests are one place where you can configure code tests to run automatically, receive comments and feedback from peers, and later approval from whoever is in charge of that repo.

The tests run on the code ensure that you didn't inadvertently break anything important with your changes. An important part of writing code is creating tests.

Let's get started!

You're going to create a git account and play around with it.

## Part I. Create your git accounts

The hardest part of this lab is making a username. It can be as boring as your name, or as exciting as @sexbobomb, one of our automation accounts. It shouldn't really be changed later, as that comes with all kinds of complications. Pick something that's really you!

GitLab is open source (mostly) and we really like it. Make your account [here](https://gitlab.com/users/sign_up). It's also worth [creating your account on GitHub](https://github.com/) since it's most commonly used for open source software, although it comes with all the privacy implications of Microsoft and OpenAI.

## Part II. Create your first repo

- [ ] Inside GitLab, go to the [new repo creation page](https://gitlab.com/projects/new) by clicking "new project" and click `create a blank project`. 

- [ ] Name it "Lost Newbs". 

- [ ] In the dropdown under namespace, click your cool new username. 

Namespace is where you could select a group, which is useful for organizations. 

- [ ] Set the access level to public 

- [ ] leave "Initialize repository with a README" checked

- [ ] check the box next to "Enable Static Application Security Testing (SAST)".

- [ ] Click "create project"

## Part III. Editing a file.

Repos usually have what's called a readme, which is the first page someone visiting the repo will see. 

It typically has a description of what the code does, how to deploy it, guidelines on contributing to the code, and information about the organization.

Documentation is an extremely important part of any code. It's worth taking the time to write a good readme!

New repos ship with a default readme which contains information about how to write and format a readme, how to set up a repo, how to use GitLab, and then a template of what a good readme might look like. Take a second and read through the default readme to get familiar. There's a lot to understand, so don't worry about what you don't know just yet. 

Now let's make this readme yours. You're currently in the repo view.

- [ ] Click on the filename of the readme to open it up.

You should now see some new buttons, including an "edit" button.

- [ ] Click the "edit" button, then click "Edit single file".

You're now in a file editor. Note that the formatting is different. The title now appears as `# lost newbs` instead of big, bold letters. Links are surrounded by square brackets \[ \] followed by parenthesis \( \). To do list boxes appear as `- [ ]`. There are some nice line numbers for easy reference.

This is known as `markdown` formatting, and you'll notice the file is named `README`.md`. The `.md` means markdown. It's a simple but very effective way to format documents, think like HTML, but much, much easier.

- [ ] Go ahead and select all the text from line 2 to the end and delete it, leaving you with just the title on line 1.

Commit messages are the summary of the changes you make, both for you and for other contributors. Each commit is like hitting "save" while editing a Word doc, but with a quick summary of what you added. 

The addage "save early, save often" applies here, too, and a general rule is that each commit should contain one single change to code. You can  `cherry pick` past commits and add them back into code if they're deleted. 

It may seem stuffy and tedious to write a message every time, but trust me, at some point when you go back and look through the history of revisions, it makes a huge difference. The ability to cherry pick means simple, concise commits add another dimension of reusability to your code.

- [ ] Write your fist commit message, something like "removed default readme template"

The target branch is probably `main`, which is fine for now. We'll get into branching later.

- [ ] Click `commit changes`.

You now have an almost blank readme! Yay.

## Part IV. Filling in the readme.

You might have been wondering, "why am I creating a Lost Newbs repo?" Me too, actually! Just kidding, this repo definitely has a purpose. First, it's going to be a way to share about ourselves. Later on, it's going to become a sort of portfolio of what you're learning!

Remember "show and tell" from like, elementary school? We want to know a little bit about you!

Go in and edit your readme again. Readmes are a little weird, there need to be blank lines between text.

- [ ] On line 3, put `## Hi, I'm @<your username>`, so for me, I'd put `## Hi, I'm @SurfingDoggo`.

- [ ] On line 5, put a few lines about you. You can keep it short and sweet, whatever you feel like.

- [ ] Put a blank line below what you just wrote, and then put `## What I want to learn`.

- [ ] Write a little bit about what technology interests you, what languages you're hoping to learn, anything you want! It could be something like "AI robots that clean up trash in national forests" or just simply "Python".

You can go back and change these answers anytime you like! At some point, they'll sync to profiles, once we make those (learn by helping us by contributing!). You can see mine [here](https://gitlab.com/SurfingDoggo/lost-newbs/-/commit/fdd2f4d5c1d4becea1398e2077a0c8ea42972c89) if you'd like.

- [ ] Write a useful commit message and commit changes

## Part V. Download the repo

We're taking a step into the more powerful parts of git, and it's time to get real nerdy on the command line.

If you're using Linux or MacOS, you're solid for this part. If you're on Windows, I recommend installing WSL (Windows System Linux) with Ubuntu for this part. It takes a few minutes but it's worth it. Look for how on your search engine of choice, and ask any questions you have in the Discord, we're here to help.

- [ ] Open up your terminal app (for WSL, assume we just mean your WSL terminal).

- [ ] Type the command `cd ~` and hit enter. CD means `change directory`, and `~` means to your user's home directory. You were *probably* already there, but just to be sure.

```
cd ~
```

Note that there are a lot of symbols, like `~`, that are hard to find on keyboard layouts in languages other than English. There are programming layouts for some languages that keep the language's layout but change the symbols for easy access for programming, but otherwise it may be easiest to change to the English QWERTY layout for easy access to the most-used symbols.

- [ ] Just for funsies, type `pwd` to see the path to your current directory. `/` is the root directory, and everything in Linux (technically Unix, Linux's dad) falls underneath it.

```
pwd
```

- [ ] Type the command `ls -lah`. `ls` means list, and it lists the contents of the current directory you're in, which right now is `~`. You'll see things like `Documents` and `Pictures` here (WSL might not include those though).

```
ls -lah
```

`-lah` contains *arguments* to the command `ls`, which modify its behavior. You can run `ls` by itself to see what the difference is.

```
ls
```

- [ ] If you want to look up what those arguments mean, type `man ls`, which brings up the instruction manual for `ls`. This works for most commands.

```
man ls
```

Type `q` to exit the manual view.

```
q
```

- [ ] After running `ls -lah`, is there a directory named `.ssh`? 

  - [ ] If not, run `mkdir .ssh`

  ```
  mkdir .ssh
  ```

  - [ ] If yes, move to the next step

- [ ] Now type `cd ~/.ssh` or just plain `cd .ssh` and hit enter. The difference is that putting the `~/` in front will *always* take you to the `.ssh` folder in your home dir, whereas `cd .ssh` takes you to the `.ssh` dir within your current working directory. The latter only works because you were already in your home directory. If you try to change into a folder that's not there, it won't work and it'll yell at you.

```
cd ~/.ssh
```

SSH is like a fancy password for interacting securely with a remote computer. You can log into one this way, but in our case, we're going to use it to authenticate with GitLab to download, or `clone`, your new repo.

- [ ] Run the following command:

```
ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/git -C "lost newbs@gitlab"
```

I'd personally change the `~/.ssh/git` to `~/.ssh/your-username`, so for me, `~/.ssh/surfingdoggo`, but as long as you remember it later, you could put `~/.ssh/oogly-boogly`. 

It's going to ask for a password. Don't type anything, just hit enter twice. 

- [ ] Run `ls -lah` again. 

```
ls -lah`
```
You should now have your own version of the following files:
```
$ ls -lah

drwxr-xr-x  2 c c 4.0K Jul  7 15:38 .
drwxr-xr-x 22 c c 4.0K Jul 24 13:31 ..
-rw-------  1 c c  411 Jun 10  2023 surfingdoggo
-rw-r--r--  1 c c  100 Jun 10  2023 surfingdoggo.pub
```

`surfingdoggo` is your private key. It is your password. Once you complete the next few steps, anyone with this file will be able to clone your repos and access your GitLab account. Treat it nice. Do not share it. Don't write it down on a sticky note to paste on your computer screen (nor would you ever want to, it's pretty long).

`surfingdoggo.pub` is what's known as a public key. You can share it on twitter. You could put it on a billboard for everyone to be real confused by on their way to work. You could give it to your worst enemy. You could send it to the Chinese government's hackers. Through complex math, the public key can be derived from the private key, but the private key can *never* be derived from the public key.

You can log into any system that has your `public key` in the right place. By sharing it with the communists, you can only ever hack *their* systems, but they can never hack yours (at least not this way). You just pulled a reverse card on the hackers. Nicely done, you *hacker* you.

So knowing that, we're going to copy the `public key` into your GitLab account so you can download your code.

- [ ] Type `cat surfingdoggo.pub` and hit enter, replacing `surfingdoggo` with the name of your key.

```
cat surfingdoggo.pub
```

```
$ cat surfingdoggo.pub

ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEqaNgHgU/Jaqa2YKjbtgwzcm1i8xgXSAeV8ielrrEa4 surfingdoggo@oenyi
```

Blegh, what is ***that***!?

It's what happens when your worst math teacher ever and your computer have a baby. Let's take it apart.

`ssh-ed25519` is the type of encryption. You'll recall in the command we put `-t ed25519` as an argument. `ssh-rsa` is another kind of encryption. It's the most common, and you'll see it all over the place, but I find it to be unwieldy. `ed25519` is more concise, so easier to copy, and *supposedly* more secure.

`AAAAC3NzaC1lZDI1NTE5AAAAIEqaNgHgU/Jaqa2YKjbtgwzcm1i8xgXSAeV8ielrrEa4` is what nightmares are made of. 

Let's pause and take a look at the private key. 

As long as you aren't somewhere public where anyone can look over your shoulder and snap a quick pick like in a coffee shop (like me right now 😬), do the following, replacing `surfingdoggo` with the name of your SSH key.

  - [ ] Run `cat surfingdoggo`, for what I'm realizing is a really funny command (what would the cats think??!!)

  ```
  cat surfingdoggo
  ```

  It puts out something like

  ```
  $ cat surfingdoggo

  -----BEGIN OPENSSH PRIVATE KEY-----
  hunter7
  -----END OPENSSH PRIVATE KEY-----
  ```

  hunter7 is a nerd joke, it's the typical "oops I shared my password" example. 

  As I'm sure you don't need me to point out, it actually has a few nightmares' combined worth of scary blah text, like an evil black cat with red eyes stampeeded all over your keyboard. It's meant to be secure and not really meant for you to look at, but demonstrates the math behind this all. The public key can be created from the private key over and over again.

  Now memorize the private key.

  KIDDING!!!!!

  When you're done staring at your private key and regretting all life decisions leading up to this point, hit `ctrl+l` (as in `ctrl`+`L` but lowercase) to clear it from view.

Okay, now back to the public key.

The last part, `surfingdoggo@oenyi`, is just a label. It comes from the `-C "your label here"` part of the `ssh-keygen` command, and can really be whatever you want. I tend to put the username @ the computer I created it on, since if I get a new computer, I just create a new SSH key instead of copying the old one over, and my laptop's name was `oenyi`.

Let's add the public key (not the private key!) to GitLab.

- [ ] In a new tab, navigate to your [GitLab user preferences](https://gitlab.com/-/profile/preferences) by clicking on your user icon to bring up the menu.

- [ ] Click on `SSH Keys`.

- [ ] Click on `Add new key`

- [ ] Under `Key`, paste your public key (not your private key!). 

```
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEqaNgHgU/Jaqa2YKjbtgwzcm1i8xgXSAeV8ielrrEa4 surfingdoggo@oenyi
```

A good way to distinguish between private and public is your public key is shorter. For `ed25519`, it's one line (for `RSA` it can be several). Your private key also says really big `-----BEGIN OPENSSH PRIVATE KEY-----`.

The `Title` gets filled in automatically from the `-C` comment, but change it as you'd like.

It gives you an expiration date as a year from the current date. I usually put mine to 31 December of the current year so I remember to change them all at the same time, but it doesn't matter a whole ton.

- [ ] Click "Add key".

Yay! Your key is now added. Close that tab and go back to the repo.

## Part VI. Cloning the repo via SSH

You should be on your repo's main page, and there should be a `Code` button. Click it, and a menu appears.

- [ ] Click to copy the `Clone with SSH` value.

```
git@gitlab.com:SurfingDoggo/lost-newbs.git
```

Note that if it starts with `https://`, you grabbed the wrong one.

Go back to your terminal.

- [ ] Type `cd ~` to return to your home folder.

```
cd ~
```

Let's make a place for your code to live.

- [ ] Type `mkdir code; cd $_`

```
mkdir code; cd $_
```

The `;` is one way to run multiple commands in one line.

The `cd $_` uses the argument from the previous command, in this case the name of the directory we created, `code`, so it changes directories into `$_`.

Before we can SSH, we need to put the private SSH key into your computer's consciousness. In other words, your SSH key is currently in the drawer with all the keys, but we can tell your computer to grab it and hold it in its hand to be ready.

- [ ] Add your SSH key to your computer's stream of consciousness:

```
ssh-add ~/.ssh/surfingdoggo
```

Except change the `surfingdoggo` to whatever you named your key.

You can view the keys in your computer's active keychain/in its hand with:

```
ssh-add -L
```

And sometimes it can be useful to clear the keys from the keychain (put them all back in the drawer) and start over:

```
ssh-add -D
```

- [ ] Clone your repo by typing `git clone ` and pasting the URL from above

```
git clone git@gitlab.com:SurfingDoggo/lost-newbs.git
```

You now cloned, or downloaded, your repo!

```
ls -lah`
```

```
ls  -lah

total 12K
drwxr-xr-x  3 c c 4.0K Jul 24 14:17 .
drwxr-xr-x 23 c c 4.0K Jul 24 14:17 ..
drwxr-xr-x  3 c c 4.0K Jul 24 14:17 lost-newbs
```

- [ ] CD into the repo

```
cd lost-newbs
```

- [ ] Run `ls` 

```
ls -lah
```

and you'll see that all your files are now local on your computer

```
$ ls -lah

drwxr-xr-x 3 c c 4.0K Jul 24 14:17 .
drwxr-xr-x 3 c c 4.0K Jul 24 14:17 ..
drwxr-xr-x 8 c c 4.0K Jul 24 14:23 .git
-rw-r--r-- 1 c c  843 Jul 24 14:17 .gitlab-ci.yml
-rw-r--r-- 1 c c  251 Jul 24 14:17 README.md
```

Yay!

## Part VII. VSCode

Go install VSCode, it's rad.

If you're using WSL, look up how to use WSL as a vscode backend. It's a bit of a pain and might make it worth it to back up your files and install Linux :b

To launch VSCode from your terminal, type `code .`

```
code .
```

If you're still inside `~/code/lost-newbs`, it'll launch VSCode in the context of that repo. You could also type `code ..`, which would launch VSCode in the context of one directory *above* the current directory. If you run `pwd`

```
pwd
```

and get back

```
$ pwd                                                                                                                       /home/surfingdoggo/code/lost-newbs
```

The parent directory of `lost-newbs` is `code`.

Run `ls`

```
ls -lah
```

and get back

```
$ ls -lah                
total 20K
drwxr-xr-x 3 c c 4.0K Jul 24 14:17 .
drwxr-xr-x 3 c c 4.0K Jul 24 14:17 ..
drwxr-xr-x 8 c c 4.0K Jul 24 14:23 .git
-rw-r--r-- 1 c c  843 Jul 24 14:17 .gitlab-ci.yml
-rw-r--r-- 1 c c  251 Jul 24 14:17 README.md
```

Notice the `.` and the `..`. The `.` always refers to the current directory, and the `..` always refers to the directory *one level above*.

If you're confused, try typing

```
code ..
```
then
```
pwd
```
then
```
ls -lah
```
Try that sequence a few times.

Eventually you'll end up in the `/` directory, or root, and you won't be able to go any further because it's the parent directory to all other directories. As they say,

> *one directory to rule them all, one directory to find them, one directory to bring them all, and in the darkness bind them*

We're going zero to nerd, get used to it. 

So anyway, back on topic. VSCode. You can now edit your files there. VSCode is great, and later on we'll get into plugins and stuff.

Remember when you edited your repo's readme in the browser and afterward did the commit message? We get to do that in the command line (CLI). Maximum nerd points.

- [ ] Open up your readme in VSCode and at the bottom put `## My Hobbies`. 

- [ ] Add a few hobbies you like.

```
## My Hobbies

Surfing, camping, traveling, film/darkroom photography, cars, reading.
```
- [ ] Save with `ctrl+s` or `cmd+s` depending on your flavor of computer

Go back to your terminal.

- [ ] Type `git status`

```
git status
```
it'll show something like
```
$ git status   

On branch main
Your branch is up to date with 'origin/main'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
        modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```
Another useful command for *modified* files (doesn't work for new files) is `git diff`
```
git diff
```
and looks something like
```
diff --git a/README.md b/README.md
index 48850dd..56c689a 100644
--- a/README.md
+++ b/README.md
@@ -10,4 +10,8 @@ Tech nerd. Surfer.

 * I'd like to learn the dark magic of frontend dev

-* It would be really cool to learn OpenCL (c++ for graphics cards)
\ No newline at end of file
+* It would be really cool to learn OpenCL (c++ for graphics cards)
+
+## My Hobbies
+
+Surfing, camping, traveling, film/darkroom photography, cars, reading.
(END)
```
Press `q` to exit that view
```
q
```
Everything in the CLI has to be specified, so let's tell git which file(s) we want to commit.
```
git add README.md
```
You can also do `git add -A` to add all files, and `git add .` to add all files in the current directory and any subdirectories. When in doubt, it's better to specify the filename to help with concise commits.

Run `git status` again

```
git status
```
and see something like
```
Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
        modified:   README.md
```
You're now ready to commit. It's the same idea as before.
```
git commit -m "your commit message here"
```
It'll spit out something like
```
$ git commit -m "my nerdy hobbies" 

[main 60fa8b2] my nerdy hobbies
 1 file changed, 5 insertions(+), 1 deletion(-)
```
You can also just run `git commit`, but using a terminal text editor is maybe overwhelming for you at this point. If you like confusing pain and also incredibly powerful text editors, search for something like "how to use VIM". It's very much worth learning if you feel like diving into the deep end.

You've commited your changes, but they're stil only local on your laptop. Let's push them to GitLab.

```
git push
```
it'll give you something like
```
$ git push

Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 20 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 386 bytes | 386.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0 (from 0)
To gitlab.com:SurfingDoggo/lost-newbs.git
   fdd2f4d..60fa8b2  main -> main
```
Now, if you go back to the repo page in your browser and refresh, your changes should be there.

Nicely done.

## Part VIII. Adding the Lost Newbs bot

We're developing the Lost Newbs bot to sync your "about you", give you badges for achievements, share feedback in code reviews, help you learn, and who knows what else. This section is coming soon!
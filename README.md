# Lost Newbs
## Becoming a Nerd
## Languages
[español](/README.es.md)
## Welcome
Congrats, you're officially a Lost Newb.

No, you can't go back! You've been knighted, initiated, christened, baptized... whatever flavor you pick, it's official. You're on the pathway to becoming a nerd.

You're where the cool kids hang out.
## Wait but... what even?
`L0$7 n3wB` (also, Lost Newb) - Someone at level zero when it comes to creating tech, but who wants to learn, for any reason, except for global domination (we're leaving that for the AI).

Are you in? Here, let's see if we can convince you.
## Technology to Change the World
